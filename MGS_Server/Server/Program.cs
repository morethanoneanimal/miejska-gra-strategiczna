﻿using Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {        
        private TcpListener tcpListener;
        private Thread listenThread;
        private static GameModel game = new GameModel("best game", null, 0);
        private static Mutex mutex = new Mutex(false);
        int count = 0;
        public Program()
        {
            //game.level = MakeLevel();
            Message lvlDetails = new Message(MessageType.LevelDetails, null, 1, MakeLevel().serialize());
            game.AddMessage(lvlDetails);
            this.tcpListener = new TcpListener(IPAddress.Any, 3000);            
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.Start();

        }
        private void ListenForClients()
        {
            this.tcpListener.Start();

            while (true)
            {
                //blocks until a client has connected to the server
                TcpClient client = this.tcpListener.AcceptTcpClient();
                Console.WriteLine("someone connected");
                //create a thread to handle communication 
                //with connected client
                Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                clientThread.Start(client);
            }
        }

        private void HandleClientComm(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream clientStream = tcpClient.GetStream();
            byte[] message = new byte[4096];
            int bytesRead;
            while (true)
            {
                bytesRead = 0;
                try
                {                    
                    bytesRead = clientStream.Read(message, 0, 4096); // rozważyć tę liczbę
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.ToString());
                    break;
                }
                if (bytesRead == 0)
                {
                    Console.WriteLine("Disconnected");
                    break;
                }                
                //message has successfully been received
                ASCIIEncoding encoder = new ASCIIEncoding();                
                // to do: przemyśleć mechanizm odbierania komunikatów dłuższych (tak jak w kliencie)
                string msgStr = System.Text.Encoding.UTF8.GetString(message).Substring(0, bytesRead);
                msgStr = msgStr.Substring(0, msgStr.IndexOf('@'));
                Console.WriteLine("Message: ");
                Console.WriteLine(msgStr);
                try
                {
                    Message m = (Message)JsonConvert.DeserializeObject<Message>(msgStr);                    
                    mutex.WaitOne(); // dodanie wiadomości do kolejki musi być synchronizowane
                    ReactOnMesssage(m);
                    IEnumerable<Message> back = processMessage(m);
                    // małpa na końcu, żeby gracz wiedział gdzie koniec transmisji      
                    string backMsg = JsonConvert.SerializeObject((IEnumerable<Message>)back) + '@';
                    byte[] payload = System.Text.Encoding.UTF8.GetBytes(backMsg);
                    clientStream.Write(payload, 0, payload.Length);
                    mutex.ReleaseMutex();
                }
                catch(Exception e)
                {
                    Console.WriteLine("Exception when parsing msg, bytes read: " + bytesRead + ", str len: " + msgStr.Length);
                    Console.WriteLine(msgStr);
                    Console.WriteLine(e.ToString());
                }
                
            }
            tcpClient.Close();
        }

        public static void ReactOnMesssage(Message m)
        {
            if (m.Type == MessageType.Connect)
            {
                if (game.messages.Where(x => x.Type == MessageType.Connect && x.Player.Login == m.Player.Login).Count() == 1)
                {
                    // grant points
                    game.AddMessage(new Message(MessageType.GivePoints, m.Player, game.Order + 1, "100"));
                }
            }
        }

        /// <summary>
        /// Na podstawie wiadomości od klienta ustalane jest ile wiadomości u klienta brakuje. 
        /// </summary>
        /// <param name="m"></param>
        /// <returns>Wiadomości, których klient nie ma</returns>
        private IEnumerable<Message> processMessage(Message m)
        {
            int missing = game.Order - (m.Order - 1); // tyle wiadomości klient nie ma
            Console.WriteLine("missing: " + missing);
            Console.WriteLine("order: " + game.Order);
            List<Message> msgs = new List<Message>();           
            msgs.AddRange(game.messages.GetRange(game.Order - missing, missing));
            // ack, żeby gracz wiedział, że już wszystkie wiadomości.
            msgs.Add(new Message(MessageType.Ack, null, game.Order + 1, count.ToString()));
            m.UpdateOrder(msgs.Last().Order);
            Console.WriteLine("returning: " + msgs.Last().Order);
            game.AddMessage(m);
            ReactOnMesssage(m);
            return msgs;            
        }

        Level MakeLevel()
        {
            Level level = new Level();
            Area komandosow = new Area();
            komandosow.Name = "Komandosów";
            komandosow.Vertices.Add(new Area.Point(50.044358, 19.931858));
            komandosow.Vertices.Add(new Area.Point(50.044351, 19.932523));
            komandosow.Vertices.Add(new Area.Point(50.044020, 19.932378));
            komandosow.Vertices.Add(new Area.Point(50.044003, 19.931997));
            level.areas.Add(komandosow);

            Area blonia = new Area();
            blonia.Name = "Błonia";
            blonia.Vertices.Add(new Area.Point(50.062540, 19.902475));
            blonia.Vertices.Add(new Area.Point(50.059289, 19.923117));
            blonia.Vertices.Add(new Area.Point(50.057112, 19.907110));
            blonia.Vertices.Add(new Area.Point(50.060226, 19.901016));
            level.areas.Add(blonia);

            Area rynek = new Area();
            rynek.Name = "Rynek i okolice";
            rynek.Vertices.Add(new Area.Point(   50.063697, 19.935949   ));
            rynek.Vertices.Add(new Area.Point(50.061879, 19.943288));
            rynek.Vertices.Add(new Area.Point(50.057305, 19.940498));
            rynek.Vertices.Add(new Area.Point(50.056837, 19.936207));
            rynek.Vertices.Add(new Area.Point(50.060446, 19.932430));
            level.areas.Add(rynek);

            Area aghP = new Area();
            aghP.Name = "AGH Wschód";
            aghP.Vertices.Add(new Area.Point(50.068225, 19.912909));
            aghP.Vertices.Add(new Area.Point(50.067991, 19.913456));
            aghP.Vertices.Add(new Area.Point(50.067636, 19.913327));
            aghP.Vertices.Add(new Area.Point(50.067791, 19.912571));
            level.areas.Add(aghP);

            Area aghL = new Area();
            aghL.Name = "AGH Zachód";
            aghL.Vertices.Add(new Area.Point(50.068253, 19.912544));
            aghL.Vertices.Add(new Area.Point(50.067791, 19.912431));
            aghL.Vertices.Add(new Area.Point(50.067871, 19.911595));
            aghL.Vertices.Add(new Area.Point(50.068597, 19.911761));
            level.areas.Add(aghL);

            Area kebab = new Area();
            kebab.Name = "AGH Zachód";
            kebab.Vertices.Add(new Area.Point(50.067636, 19.912936));
            kebab.Vertices.Add(new Area.Point(50.067592, 19.913236));
            kebab.Vertices.Add(new Area.Point(50.067492, 19.913231));
            kebab.Vertices.Add(new Area.Point(50.067454, 19.912850));
            level.areas.Add(kebab);

            Area komandosow2 = new Area();
            komandosow2.Name = "kom2";
            komandosow2.Vertices.Add(new Area.Point(50.044389, 19.930109));
            komandosow2.Vertices.Add(new Area.Point(50.044502, 19.931622));
            komandosow2.Vertices.Add(new Area.Point(50.043793, 19.931809));
            komandosow2.Vertices.Add(new Area.Point(50.043427, 19.929524));
            level.areas.Add(komandosow2);



            return level;
        }
            
        
      
        static void Main(string[] args)
        {
            Program p = new Program();
            Console.WriteLine("hi !");            
            Console.ReadKey();
            
        }
    }
}
