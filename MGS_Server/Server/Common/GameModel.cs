﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Common
{
    /// <summary>
    /// Wszystko, co jest potrzebne by przeprowadzić rozgrywkę.
    /// Lista graczy, adres i port serwera
    /// </summary>
    public class GameModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public List<Player> players = new List<Player>(); //to do: ukryty dostęp
        public Player Self
        { get; protected set; }
        public List<Message> messages; //to do: ukryty dostęp
        public Level level; //to do: ukryty dostęp
        public Chat chat = new Chat(); //to do: ukryty dostęp
        public GameModel(string name, string address, int port)
        {
            this.Name = name;
            this.Address = address;
            this.Port = port;
            messages = new List<Message>();
            level = new Level();
        }

        /// <summary>
        /// Tu odbywa się główna logika, reakcja na wiadomości
        /// </summary>
        /// <param name="m"></param>
        public void AddMessage(Message m)
        {
            if (m.Type == MessageType.Ack)
                return; // nic sie z tym nie robi, do listy sie tego tez nie dodaje
            else if(m.Type == MessageType.Connect)
            {
                if (GetPlayerByLogin(m.Player.Login) == null)
                    players.Add(m.Player);
            }
            else if (m.Type == MessageType.Location)
            {
                Player p = GetPlayerByLogin(m.Player.Login);
                if (p != null)
                {
                    p.x = m.Player.x;
                    p.y = m.Player.y;
                    p.CurrentArea = null;
                    foreach (Area a in level.areas)
                    {
                        if (a.IsPointInArea(new Area.Point(p.x, p.y)))
                        {
                            p.CurrentArea = a;
                            break;
                        }
                    }
                }
                else
                {
                    Debug.WriteLine("Error, no player " + m.Player.ToString());
                }
            }
            else if (m.Type == MessageType.TakeArea)
            {
                TakeArea(m);
            }
            else if(m.Type == MessageType.Echo)
            {
                chat.AddMessage(m);
            }
            else if(m.Type == MessageType.LevelDetails)
            {
                this.level = Level.deserialize(m.Data);
                Debug.WriteLine("Level set");
            }
            else if(m.Type == MessageType.GivePoints)
            {
                Player p = GetPlayerByLogin(m.Player.Login);
                int add = Int32.Parse(m.Data);
                p.points += add;
            }
            messages.Add(m);
        }

        public void TakeArea(Message m)
        {
            Player p = GetPlayerByLogin(m.Player.Login);
            string[] parts = m.Data.Split(':');
            Area a = GetAreaByName(parts[0]);

            int atackPoints = 0;
            if (m.Data.Length > 0)
                atackPoints = Int32.Parse(parts[1]);
            a.TakeArea(p, atackPoints);
        }

        public Player GetPlayerByLogin(string login)
        {
            return players.Where(x => x.Login == login).FirstOrDefault();
        }

        public Area GetAreaByName(string name)
        {
            return level.areas.Where(x => x.Name == name).FirstOrDefault();
        }

                

        public void SetSelfPlayer(String login)
        {
            Player p = new Player(login);
            players.Add(p);
            Self = p;
        }

        protected void notify(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        protected string _address;
        public string Address
        {
            get
            {
                return _address;
            }
            set 
            {
                _address = value;
                notify("address");
            }
        }

        public int Order
        {
            get
            {
                return messages.Count;
            }
        }
           
      
        protected string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                notify("name");
            }
        }


        protected int _port;
        public int Port
        {
            get { return _port; }
            set
            {
                _port = value;
                notify("port");
            }
        }

        public string Description
        {
            get { return Name + ", " + Address + ":" + Port; }           
        }

        public void processMessages(IEnumerable<Message> m)
        {
            foreach(Message msg in m)
            {      
                AddMessage(msg);
            }
        }
    }
}
