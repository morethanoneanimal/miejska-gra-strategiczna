﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Windows.Storage;
using Common;

namespace ClientApp
{
    /// <summary>
    /// Klasa do zarządzania plikiem konfiguracyjnym programu.
    /// W pliku konfiguracyjnym siedzą takie rzeczy, jak login gracza i dane gry.
    /// </summary>
    public class FileManager
    {
        private StorageFolder localFolder;
        public const string configFile = "config.dat";
        public FileManager()
        {
            localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
        }

        public async Task<GameModel> findGameByName(string name)
        {
            ObservableCollection<GameModel> list = await getAvaliableGames();
            return list.Where(x => x.Name == name).First();
        }

        public async Task<ObservableCollection<GameModel>> getAvaliableGames()
        {
            ObservableCollection<GameModel> toReturn = new ObservableCollection<GameModel>();
            StorageFile file = null;
            try
            {
                file = await localFolder.GetFileAsync(configFile);
            }
            catch(Exception e)
            {
                // plik nie istnieje, prawdopodobnie
                return toReturn;
            }            
            using(StreamReader s = new StreamReader(await file.OpenStreamForReadAsync()))
            {
                string line;
                while((line = await s.ReadLineAsync()) != null)
                {
                    string[] tab = line.Split(',');
                    toReturn.Add(new GameModel(tab[0], tab[1], Int32.Parse(tab[2])));
                }
            }

            return toReturn;     
        }

        public async Task saveAvaliableGames(Collection<GameModel> games)
        {
            var file = await localFolder.CreateFileAsync(configFile, CreationCollisionOption.OpenIfExists);
 
            using(StreamWriter s = new StreamWriter(await file.OpenStreamForWriteAsync()))
            {
                foreach (GameModel model in games)
                    await s.WriteLineAsync(model.Name + "," + model.Address + "," + model.Port);
            }

        }
    }
}
