﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using Microsoft.Phone.Notification;
using System.Threading;
using System.Diagnostics;
using Windows.Devices.Geolocation;
using Common;
using Microsoft.Phone.Net.NetworkInformation;
using System.Device.Location;
using Microsoft.Phone.Maps.Controls;

namespace ClientApp
{
    public partial class GamePage : PhoneApplicationPage
    {
        protected FileManager fileManager = new FileManager();
        protected string gameName;
        Geolocator geolocator;
        
        MapLayer mapLayer;
        MapPainter mapPainter;
        public GamePage()
        {
            InitializeComponent();
            PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            App.initGeolocator();            
            App.Geolocator.PositionChanged += geolocator_PositionChanged;
            App.Geolocator.StatusChanged += geolocator_StatusChanged;
            DeviceNetworkInformation.NetworkAvailabilityChanged += DeviceNetworkInformation_NetworkAvailabilityChanged;
            UpdateNetworkInfo();
            UpdateGeolocatorInfo();
            mapPainter = new MapPainter(MainMap);                     
        }

        void DeviceNetworkInformation_NetworkAvailabilityChanged(object sender, NetworkNotificationEventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
                {
                    UpdateNetworkInfo();
                });
        }

        private void UpdateNetworkInfo()
        {
            txtNetworkStatus.Text = "Internet connection: " + (DeviceNetworkInformation.IsNetworkAvailable ? "yes" : "no");
        }

        private void UpdateGeolocatorInfo()
        {
            txtGeolocatorStatus.Text = "GPS Status: " + (App.Geolocator.LocationStatus.ToString());
        }

        async void geolocator_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {      
            Dispatcher.BeginInvoke(() =>
                {
                    UpdateGeolocatorInfo();
                    MainMap.ZoomLevel = 13;
                }); 
        }

        private async void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            try
            {
                Debug.WriteLine("location changed..");                
                System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    Debug.WriteLine("location changed.. inside");
                    var pos = CoordinateConverter.ConvertGeocoordinate(args.Position.Coordinate);
                    MainMap.Center = pos;
                    App.GameModel.Self.SetCoordinates(pos.Latitude, pos.Longitude);
                    txtInfo.Text = "Position: " + args.Position.Coordinate.Longitude + " x " + args.Position.Coordinate.Latitude;
                    if (App.msgMutex.WaitOne(100))
                    {
                        Message msg = new Message(MessageType.Location, App.GameModel.Self, App.GameModel.Order + 1, args.Position.Coordinate.Longitude.ToString() + "x" + args.Position.Coordinate.Latitude);
                        App.sendMessage(msg);
                        var q = App.GameModel.messages;
                        afterSendMsg();
                    }
                    else
                    {

                    }
                    updateMap();
     
                });  
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        protected void updatePoints()
        {
            txtPoints.Text = "Points: " + App.GameModel.Self.points;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            string login;
            if (NavigationContext.QueryString.TryGetValue("name", out gameName))
            {                
                if (!NavigationContext.QueryString.TryGetValue("login", out login))
                {
                    login = "LoginMissing";
                }
                await init(gameName, login);                               
            }
        }

        protected async Task init(string ganeName, string login)
        {
            if (App.GameModel == null)
            {
                App.GameModel = await fileManager.findGameByName(gameName);               
                App.GameModel.SetSelfPlayer(login);
            }
            if(App.TcpManager == null)
                App.TcpManager = new TcpManager(App.GameModel);
            string result = "connect: " + await App.TcpManager.Connect();
            mmm.Text = result;

            App.msgMutex.WaitOne();
            Message hello = new Message(MessageType.Connect, App.GameModel.Self, App.GameModel.Order + 1, "");            
            App.sendMessage(hello);
            afterSendMsg();
          
            mmm.Text = result;
        }

        protected void updateMap()
        {
            mapPainter.Update();
        }


        private void PhoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Enabled;
        }

        private void btnTakeArea_Click(object sender, RoutedEventArgs e)
        {            
            if(App.GameModel.Self.CurrentArea == null)
            {
                MessageBox.Show("You're in the middle of nowhere", "Take area", MessageBoxButton.OK);
            }
            else
            {
                App.msgMutex.WaitOne();
                lblBeat.Text = "Point to beat: " + App.GameModel.Self.CurrentArea.DefensePoints;
                pointsPopup.IsOpen = true;
            }            
        }

        protected void UpdateAll()
        {
            updateMap();
            updatePoints();
        }

        protected void afterSendMsg()
        {
            UpdateAll();
            App.msgMutex.ReleaseMutex();            
        }

        private void btnChat_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/ChatPage.xaml", UriKind.Relative));
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            App.refresh();
            UpdateAll();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            afterSendMsg();
            pointsPopup.IsOpen = false;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            App.sendMessage(new Message(MessageType.TakeArea, App.GameModel.Self, App.GameModel.Order + 1, App.GameModel.Self.CurrentArea.Name + ":" + txtPointsInput.Text));
            afterSendMsg();
            pointsPopup.IsOpen = false;
        }

    }
}