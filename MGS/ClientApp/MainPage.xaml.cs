﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ClientApp.Resources;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Phone.Reactive;
using System.Collections.ObjectModel;
using System.Windows.Controls.Primitives;
using Common;

namespace ClientApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        FileManager fileManager;
        ObservableCollection<GameModel> allGames;

        public MainPage()
        {
            InitializeComponent();
            fileManager = new FileManager();

            init();
        }

        private async void init()
        {
            allGames = await fileManager.getAvaliableGames();
            gameList.ItemsSource = allGames;
            newGamePopup.IsOpen = allGames.Count == 0;
        }

        private void btnSimple_Click(object sender, RoutedEventArgs e)
        {
            newGamePopup.IsOpen = true;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            allGames.Add(new GameModel("<unknown>", txtIp.Text, Int32.Parse(txtPort.Text)));
            newGamePopup.IsOpen = false;
        }



        private async void gameList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gameList.SelectedItem != null)
            {
                await fileManager.saveAvaliableGames(allGames);
                LongListSelector sel = (LongListSelector)sender;
                GameModel model = (GameModel)gameList.SelectedItem;
                connectToGame(model);
            }
        }

        private void connectToGame(GameModel model)
        {
            gameList.SelectedItem = null;
            if (txtLogin.Text.Length == 0)
                MessageBox.Show("Tell us your login", "login", MessageBoxButton.OK);
            else
            {
                NavigationService.Navigate(new Uri("/GamePage.xaml?name=" + model.Name + "&login=" + txtLogin.Text, UriKind.Relative));
            }
        }

        private void txtIp_TextChanged(object sender, TextChangedEventArgs e)
        {
            newGameDataCorrect();
        }

        private bool newGameDataCorrect()
        {
            if (txtPort.Text.Length > 0 && txtIp.Text.Length > 0)
                btnOk.IsEnabled = true;
            else
                btnOk.IsEnabled = false;
            return btnOk.IsEnabled;
        }

        private void txtPort_TextChanged(object sender, TextChangedEventArgs e)
        {
            newGameDataCorrect();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            newGamePopup.IsOpen = false;
        }

        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            await fileManager.saveAvaliableGames(allGames);
        }

        private async void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            allGames.Clear();
            await fileManager.saveAvaliableGames(allGames);
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {

        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            newGamePopup.IsOpen = true;

        }

        private async void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            await fileManager.saveAvaliableGames(allGames);
        }

    }
}