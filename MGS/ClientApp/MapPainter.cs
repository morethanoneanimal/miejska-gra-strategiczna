﻿using Common;
using Microsoft.Phone.Maps.Controls;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;


namespace ClientApp
{
    public class MapPainter
    {
        protected Map map;
        protected MapLayer mapLayer;
        protected MapOverlay overlay;
        public MapPainter(Map map)
        {
            this.map = map;
            overlay = new MapOverlay();
            mapLayer = new MapLayer();
            mapLayer.Add(overlay);
            map.Layers.Add(mapLayer);
        }

        public void Update()
        {
            mapLayer.Clear();
            map.MapElements.Clear();    // todo: it can be done just once ...
            foreach(Area a in App.GameModel.level.areas)
            {
                MapOverlay over = new MapOverlay();
                MapPolygon polygon = new MapPolygon();
                GeoCoordinateCollection collection = new GeoCoordinateCollection();
                foreach(var p in a.Vertices)
                    collection.Add(new GeoCoordinate(p.X, p.Y));
                polygon.Path = collection;
                Color fillColor = Color.FromArgb(128, 20, 20, 255); // neutral
                if (a.Owner == App.GameModel.Self)
                    fillColor = Color.FromArgb(128, 20, 255, 20);
                else if (a.Owner != null)
                    fillColor = Color.FromArgb(128, 200, 0, 0);

                polygon.FillColor = fillColor;
                map.MapElements.Add(polygon);
            }
            foreach(Player p in App.GameModel.players)
            {
                MapOverlay over = new MapOverlay();
                Ellipse circle = new Ellipse();
                circle.Height = 15;
                circle.Width = 15;
                circle.Opacity = 50;
                circle.Fill = p == App.GameModel.Self ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Orange);

                over.Content = circle;
                over.PositionOrigin = new Point(0.5, 0.5);
                over.GeoCoordinate = new GeoCoordinate(p.x, p.y);

                mapLayer.Add(over);             
            }

        }
    }
}
