﻿using Common;
using Microsoft.Phone.Net.NetworkInformation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace ClientApp
{
    /// <summary>
    /// Ukrywa "niskopoziomowe" detale komunikacji z serwerem.
    /// </summary>
    public class TcpManager
    {
        protected Socket socket;
        protected GameModel game;
        protected ManualResetEvent a = new ManualResetEvent(false);
        string result;
        ManualResetEvent res = new ManualResetEvent(false);
        private IEnumerable<Message> missingMessages;

        string exclusiveObject = "asdf";

        public TcpManager(GameModel m)
        {
            game = m;            
        }

        public bool isNetworkConnection()
        {
            return DeviceNetworkInformation.IsNetworkAvailable;
        }

        public Task<string> Connect()
        {
            if (!isNetworkConnection())
                return null;
            if (socket != null && socket.Connected)
            {
                Debug.WriteLine("Already connected");
                return new Task<string>(() => { return "already connected"; });
            }
            DnsEndPoint endPoint = new DnsEndPoint(game.Address, game.Port);
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            SocketAsyncEventArgs args = new SocketAsyncEventArgs()
            {
                RemoteEndPoint = endPoint                
            };
            args.Completed += args_Completed;

            socket.ConnectAsync(args);
            Task<string> t = new Task<string>(() => {
                a.WaitOne();
                return result;
            });
            t.Start();
            return t;
        }

        public async Task<IEnumerable<Message>> SendAsync(Message m)
        {
            if (!isNetworkConnection())
                return null;
            string v = JsonConvert.SerializeObject(m) + '@';
            Debug.WriteLine("v " + v.Length + ": " + v);
            byte[] payload = System.Text.Encoding.UTF8.GetBytes(v);
            Debug.WriteLine("paylaod: " + payload.Length);
            Monitor.Enter(exclusiveObject);            
            Task<string> task = SendAsync(payload);
            Monitor.Wait(exclusiveObject);
            task.Wait();
            var copy = missingMessages;
            missingMessages = null;
            Monitor.Exit(exclusiveObject);
            return copy;
        }

        public async Task<IEnumerable<Message>> RecvMessages()
        {
            string msg = "";
            bool eom = false;
            while (!eom)
            {
                string part = await RecvAsync();
                if (part.Contains('@'))
                {
                    eom = true;
                    part = part.Substring(0, part.IndexOf('@'));
                }
                msg += part;
            }
            if (msg == null)
                return null;
            try
            {
                IEnumerable<Message> msgs = JsonConvert.DeserializeObject<IEnumerable<Message>>(msg);
                return msgs;
            }
            catch(Exception e)
            {
                Debug.WriteLine("exception while deserializing");
                Debug.WriteLine(msg);
                return null;
            }
        }

        public Task<string> SendAsync(byte[] payload)
        {
            Task<string> t = new Task<string>(() =>
            {
                if (!isNetworkConnection())
                    return null;
                string response = null;
                try
                {
                    
                    SocketAsyncEventArgs e = new SocketAsyncEventArgs()
                    {
                        RemoteEndPoint = socket.RemoteEndPoint               
                    };
                    e.Completed += e_Completed;
           
                    e.SetBuffer(payload, 0, payload.Length);
                    socket.SendAsync(e);
                    
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.ToString());
                    return "error";
                }
                return response;
            });
            t.Start();
            return t;
        }

        async void e_Completed(object sender, SocketAsyncEventArgs e)
        {
            result = e.SocketError.ToString();
            Debug.WriteLine("bytes transfered: " + e.BytesTransferred);
            lock (exclusiveObject)
            {
                Task<IEnumerable<Message>> task = RecvMessages();
                task.Wait();
                missingMessages = task.Result;
                Monitor.PulseAll(exclusiveObject);
            }
        }

        public Task<string> RecvAsync()
        {
            Task<string> t = new Task<string>(() =>
            {
                if (!isNetworkConnection())
                    return null;
                string response = null;
                try
                {
                    ManualResetEvent mutex = new ManualResetEvent(false);                    
                    SocketAsyncEventArgs e = new SocketAsyncEventArgs()
                    {
                        RemoteEndPoint = socket.RemoteEndPoint
                    };
                    e.SetBuffer(new byte[1024], 0, 1024);
                    e.Completed += new EventHandler<SocketAsyncEventArgs>(delegate(object sender, SocketAsyncEventArgs args)
                        {
                            if (args.SocketError == SocketError.Success)
                            {
                                response = Encoding.UTF8.GetString(e.Buffer, e.Offset, e.BytesTransferred);                                
                            }
                            else
                            {
                                response = e.SocketError.ToString();
                            }
                            mutex.Set();
                        });
                    socket.ReceiveFromAsync(e);
                    mutex.WaitOne();
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.ToString());
                    return null;
                }
                return response;
            });
            t.Start();
            return t;
        }

        void args_Completed(object sender, SocketAsyncEventArgs e)
        {
            result = e.SocketError.ToString();
            a.Set();
        }


    }
}
