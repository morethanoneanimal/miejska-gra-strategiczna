﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Common;

namespace ClientApp
{
    public partial class ChatPage : PhoneApplicationPage
    {
        bool scrollFlag = false;
        public ChatPage()
        {
            InitializeComponent();
            llsMessages.ItemsSource = App.GameModel.chat.Messages;
            
        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            App.msgMutex.WaitOne();
            Message msg = new Message(MessageType.Echo, App.GameModel.Self, App.GameModel.Order + 1, txtMsg.Text);
            txtMsg.Text = "";
            App.sendMessage(msg);
            App.msgMutex.ReleaseMutex();
        }

        private void llsMessages_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            if (scrollFlag)
                scrollFlag = false;
            else
            {
                //llsMessages.ScrollTo(App.GameModel.chat.Messages.Last());
                scrollFlag = true;
            }

        }
    }
}