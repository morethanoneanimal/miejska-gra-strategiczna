﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Common
{
    public class Level
    {
        public List<Area> areas;

        public string serialize()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static Level deserialize(string str)
        {
            return JsonConvert.DeserializeObject<Level>(str);
        }
    }
}
