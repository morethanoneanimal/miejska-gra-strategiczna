﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Common
{
    public class Area
    {
        /// <summary>
        /// Wierzchołki muszą być podawane po kolei, wielokąt musi być wypukły
        /// </summary>
        private List<Point> vertices;
        public string Name;
        public Player Owner;

        bool IsPointInArea(Point _point)
        {
            double x = _point.X;
            double y = _point.Y;
            int i, j;
            int pointsNumber = vertices.Count;
            Point[] points = vertices.ToArray();
            bool c = false;
            for (i = 0, j = pointsNumber - 1; i < pointsNumber; j = i++)
            {
                if (((points[i].Y > y) != (points[j].Y > y)) &&
                 (x < (points[j].X - points[i].X) * (y - points[i].Y) / (points[j].Y - points[i].Y) + points[i].X))
                    c = !c;
            }
            return c;
        }

        public List<Point> Vertices
        {
            set { this.vertices = value; }
            get { return this.vertices; }
        }

        public class Point
        {
            double _x;
            double _y;

            public Point(double x, double y)
            {
                _x = x;
                _y = y;
            }

            public double X
            {
                set { this._x = value; }
                get { return this._x; }
            }

            public double Y
            {
                set { this._y = value; }
                get { return this._y; }
            }
        }

    }
}
