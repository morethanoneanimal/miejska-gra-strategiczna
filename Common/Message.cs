﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Message
    {
        public MessageType Type
        { get; set; }
        public Player Player
        { get; set; }
        public int Order
        { get; protected set; }

        public string Data
        { get; protected set; }

        public Message(MessageType t, Player p, int order, string data)
        {
            Type = t;
            Player = p;
            Order = order;
            Data = data;
        }

        public byte[] GetPayload()
        {
            string s = Type + ";" + Player + ";" + Order + ";" + Data;
            return Encoding.UTF8.GetBytes(s);
        }
        
        public void UpdateOrder(int order)
        {
            Order = order;
        }

    }

    public enum MessageType
    {
        Connect, Disconnect, Echo, Missing, Location, Ack
    }
}
