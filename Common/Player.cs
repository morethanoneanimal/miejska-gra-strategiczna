﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Player
    {
        public string Login
        { get; protected set; }

        public double x;
        public double y;

        public void SetCoordinates(double _x, double _y)
        {
            x = _x;
            y = _y;
        }

        public Player(string login)
        {
            Login = login;
        }

        public override string ToString()
        {
            return Login;
        }
    }
}
