# Miejska gra strategiczna #

Gra, w której gracze poruszają się w realnej przestrzeni i zdobywają sektory.

Składa się z serwera oraz z aplikacji mobilnej na Windows Phone.


Screen z aplikacji mobilnej:

![Tak to wygląda](http://student.agh.edu.pl/~danek/mgs.png)

### Funkcje ###
* Komunikacja niskopoziomowa, oparta na socketach
* Algorytm synchronizujący stan u wszystkich klientów, gwarantujący kolejność komunikatów